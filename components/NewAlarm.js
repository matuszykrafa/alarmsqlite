import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Vibration, Dimensions } from 'react-native';

class NewAlarm extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Nowy alarm",
        headerStyle: {
            backgroundColor: "#4e40b8",
        },
        headerTitleStyle: {
            color: "#eeeeee"
        },
        headerTintColor: '#eeeeee',
        headerRight: <TouchableOpacity style={{ marginRight: 20 }} onPress={navigation.getParam('addAlarm2')}>
            <Text style={{ color: "#eeeeee", fontSize: 18, fontWeight: "bold" }}>Zapisz</Text>
        </TouchableOpacity>
    })
    constructor(props) {
        super(props);
        this.state = {
            circle: 'hour',
            hour: "12",
            minute: "00"
        };
        this.save = this.save.bind(this);
    }
    componentDidMount() {
        this.props.navigation.setParams({ addAlarm2: this.save })
    }
    save() {
        this.props.navigation.state.params.addAlarm(this.state.hour, this.state.minute);
        this.props.navigation.goBack();
    }
    changeCircle(a) {
        Vibration.vibrate();
        this.setState({
            circle: a
        })
    }
    setHour(hour) {
        Vibration.vibrate();
        if (hour.toString().length > 1)
            this.setState({
                hour: hour
            })
        else
            this.setState({
                hour: '0' + hour.toString()
            })
    }
    setMinute(minute) {
        Vibration.vibrate();
        if (minute.toString().length > 1)
            this.setState({
                minute: minute
            })
        else
            this.setState({
                minute: '0' + minute.toString()
            })

    }
    render() {
        var step = 2 * Math.PI / 12;  // see note 1
        var h = Dimensions.get('window').width / 2;
        var k = Dimensions.get('window').height / 4;
        var r = 170;
        var hours1 = []
        for (let i = 1; i <= 12; i++) {
            hours1.push(
                <TouchableOpacity
                    onPress={() => { this.setHour(Math.abs(13 - i)) }}
                    key={Math.abs(13 - i)}
                    style={{ position: "absolute", left: h + r * Math.cos((i + 2) * step), top: k - r * Math.sin((i + 2) * step) }}>
                    <Text style={{ fontSize: 24, color: 'black' }}>{Math.abs(13 - i)}</Text>
                </TouchableOpacity>)
        }
        r = 130;
        var hours2 = []
        for (let i = 1; i <= 12; i++) {
            hours2.push(
                <TouchableOpacity onPress={() => { this.setHour(Math.abs(13 - i) + 12) }} key={Math.abs(13 - i) + 12} style={{ position: "absolute", left: h + r * Math.cos((i + 2) * step), top: k - r * Math.sin((i + 2) * step) }}>
                    <Text style={{ fontSize: 24, color: 'black' }}>{Math.abs(13 - i) + 12}</Text>
                </TouchableOpacity>)
        }
        r = 170;
        var minutes = []
        for (let i = 1; i <= 12; i++) {
            minutes.push(
                <TouchableOpacity onPress={() => { this.setMinute(Math.abs(12 - i) * 5) }} key={Math.abs(13 - i)} style={{ position: "absolute", left: h + r * Math.cos((i + 3) * step), top: k - r * Math.sin((i + 3) * step) }}>
                    <Text style={{ fontSize: 24, color: 'black' }}>
                        {(Math.abs(12 - i) * 5).toString().length > 1
                            ? Math.abs(12 - i) * 5
                            : '0' + (Math.abs(12 - i) * 5)

                        }</Text>
                </TouchableOpacity>)
        }
        return (
            <View style={{ flex: 1, backgroundColor: "#d8daff" }}>
                <View style={{ height: 200, flexDirection: "row", justifyContent: "space-around" }}>
                    <TouchableOpacity onPress={() => { this.changeCircle('hour') }}><Text style={{ fontSize: 50 }}>{this.state.hour}</Text></TouchableOpacity>
                    <Text style={{ fontSize: 50 }}>:</Text>
                    <TouchableOpacity onPress={() => { this.changeCircle('minute') }}><Text style={{ fontSize: 50 }}>{this.state.minute}</Text></TouchableOpacity>
                </View>
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    {this.state.circle == 'hour'
                        ?
                        <React.Fragment>
                            {hours1}
                            {hours2}

                        </React.Fragment>
                        :
                        <React.Fragment>
                            {minutes}
                        </React.Fragment>
                    }
                </View>
            </View>
        );
    }
}

export default NewAlarm;
