import React, { Component } from 'react';
import { View, Text, Vibration } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Item from './Item';
import Database from './Database';

class Main extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Alarmy",
        headerStyle: {
            backgroundColor: "#4e40b8",
        },
        headerTitleStyle: {
            color: "#eeeeee"
        },
        headerTintColor: '#eeeeee',
        headerRight: <TouchableOpacity style={{ marginRight: 20 }} onPress={navigation.getParam('goToNewAlarm')}>
            <Text style={{ color: "#eeeeee", fontSize: 18, fontWeight: "bold" }}>Dodaj</Text>
        </TouchableOpacity>
    })
    constructor(props) {
        super(props);
        this.state = {
            alarms: [],
            activeAlarms: []
        }
        this.refreshAlarms = this.refreshAlarms.bind(this);
        this.addAlarm = this.addAlarm.bind(this);
        this.goToNewAlarm = this.goToNewAlarm.bind(this);
        this.funcDelete = this.funcDelete.bind(this);
        this.funcSwitch = this.funcSwitch.bind(this);
    }
    componentDidMount() {
        this.props.navigation.setParams({ goToNewAlarm: this.goToNewAlarm })
        this.refreshAlarms();
        var that = this;
        setInterval(function () {
            var hours = new Date().getHours();
            var min = new Date().getMinutes();
            if (min.toString().length < 2) {
                min = '0' + min.toString();
            }
            that.state.activeAlarms.forEach(alarm => {
                // console.log(hours, min);
                // console.log(alarm.hour, alarm.minute)
                if (alarm.hour == hours.toString() && alarm.minute == min.toString()) {
                    // console.log("???")
                    Vibration.vibrate(100);
                }
            });
        }, 500);
    }
    funcDelete(id) {
        Database.remove(id);
        this.refreshAlarms();
    }
    funcSwitch(data, id) {
        Database.updateSwitch(data, id)
        this.refreshAlarms();
    }
    async refreshAlarms() {
        var dbData = await Database.getAll();
        var data = JSON.parse(dbData).rows._array
        var active = []
        var tab = data.map((item, index) => {
            if (JSON.parse(item.data).switched) active.push(JSON.parse(item.data))
            return <Item key={item.id} id={item.id} data={JSON.parse(item.data)} funcDelete={this.funcDelete} funcSwitch={this.funcSwitch} />
        })
        this.setState({
            alarms: tab,
            activeAlarms: active
        })
    }
    addAlarm(h, m) {
        Database.add(h, m);
        this.refreshAlarms();
    }
    goToNewAlarm() {
        this.props.navigation.navigate("newalarm", { addAlarm: this.addAlarm })
    }

    render() {
        return (
            <View style={{ backgroundColor: "#d8daff", flex: 1 }}>
                <ScrollView>
                    {this.state.alarms}
                </ScrollView>
            </View>
        );
    }
}

export default Main;
