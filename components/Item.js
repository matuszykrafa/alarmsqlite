import React, { Component } from 'react';
import { View, Text, Image, Animated } from 'react-native';
import { Switch, TouchableNativeFeedback } from 'react-native-gesture-handler';
import { addListener } from 'expo/build/Updates/Updates';
import Database from './Database';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: new Animated.Value(50), // początkowa wartość wysokości itema
            expanded: false, // zwinięty
            days: this.props.data.days,
            hour: this.props.data.hour,
            minute: this.props.data.minute
        };

        this.toValue = 0
        this.toggle = this.toggle.bind(this);
        this.toggleDay = this.toggleDay.bind(this);
    }
    toggle() {
        if (!this.state.expanded) this.toValue = 100
        else this.toValue = 50

        Animated.spring(this.state.height, {
            toValue: this.toValue,
        }).start();

        this.setState({
            expanded: !this.state.expanded
        })
    }
    toggleDay(i) {
        var tab = this.state.days.map((item, index) => {
            if (i == index) {
                var t = item;
                t.clicked = !item.clicked
                return t;
            }
            else return item;
        })
        this.setState({
            days: tab
        })
    }
    render() {
        var tab = this.state.days.map((item, index) => {
            return (
                <TouchableNativeFeedback
                    key={index}
                    background={TouchableNativeFeedback.Ripple('rgba(255,255,255,1)', true)}
                    onPress={() => this.toggleDay(index)}
                    style={{ width: 30, height: 30, }}>
                    {item.clicked
                        ?
                        <Text style={{ flex: 1, fontSize: 18, backgroundColor: "rgba(255,255,255,0.7)", borderRadius: 15, textAlign: "center", textAlignVertical: "center" }}>{item.day}</Text>
                        :
                        <Text style={{ flex: 1, fontSize: 18, textAlign: "center", textAlignVertical: "center" }}>{item.day}</Text>
                    }
                </TouchableNativeFeedback>
            )
        })
        var tab2 = this.state.days.map((item, index) => {
            if (item.clicked)
                return (
                    <Text key={index} style={{ fontSize: 18, textAlign: "center", textAlignVertical: "center" }}>{item.day}, </Text>
                )
        }).filter(item => item != undefined)
        return (
            <View style={{ margin: 10, padding: 10, borderBottomColor: "darkgray", borderBottomWidth: 1 }}>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: "space-between", paddingLeft: 20, paddingRight: 20 }}>
                    <Text style={{ fontSize: 64 }}>{this.state.hour}:{this.state.minute}</Text>
                    <Switch onValueChange={() => this.props.funcSwitch(this.props.data, this.props.id)} value={this.props.data.switched} />
                </View>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: "space-between", paddingLeft: 20, paddingRight: 20 }}>
                    <TouchableNativeFeedback
                        background={TouchableNativeFeedback.Ripple('rgba(255,255,255,1)', true)}
                        onPress={() => this.props.funcDelete(this.props.id)}
                        style={{ width: 25, height: 25, }}>
                        <Image style={{ width: 25, height: 25 }} source={require('../assets/images/garbage.png')} />
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback
                        background={TouchableNativeFeedback.Ripple('rgba(255,255,255,1)', true)}
                        onPress={this.toggle}
                        style={{ width: 25, height: 25, }}>
                        {this.state.expanded
                            ?
                            <Image style={{ width: 25, height: 25 }} source={require('../assets/images/up-arrow.png')} />
                            :
                            <Image style={{ width: 25, height: 25 }} source={require('../assets/images/down-arrow.png')} />
                        }

                    </TouchableNativeFeedback>
                </View>
                <Animated.View style={{ height: this.state.height, margin: 25, marginTop: 50, marginBottom: 0, }}>
                    {this.state.expanded
                        ?
                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                            {tab}
                        </View>

                        :
                        <View style={{ flexDirection: "row" }}>
                            {tab2}
                        </View>
                    }

                </Animated.View>
            </View>
        );
    }
}

export default Item;
