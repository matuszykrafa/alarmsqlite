import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import * as Font from "expo-font";
import MyButton from './MyButton';
import Database from "./Database";

class Start extends Component {
    static navigationOptions = {
        header: null,
    }
    constructor(props) {
        super(props);
        this.state = {
            fontloaded: false
        }
        this.handleStart = this.handleStart.bind(this)
    }
    componentWillMount = async () => {
        await Font.loadAsync({
            'myfont': require('../assets/fonts/SpaceMono-Regular.ttf'),
        });
        this.setState({ fontloaded: true })
    }
    componentDidMount() {
        Database.createTable();
    }
    handleStart() {
        this.props.navigation.navigate("main")
    }
    render() {
        return (
            this.state.fontloaded
                ?
                <React.Fragment>
                    <View style={{ flex: 1, backgroundColor: "#4e40b8", justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ fontSize: 48, color: "#eeeeee", fontFamily: 'myfont' }}> AlarmSqlite </Text>
                        <Text style={{ fontSize: 24, color: "#eeeeee", fontFamily: 'myfont' }} > alarmy </Text>
                        <View style={{ width: "90%" }}><MyButton width="100%" text="Start" click={this.handleStart} /></View>
                    </View>
                </React.Fragment>
                :
                <React.Fragment>
                    <View style={{ flex: 1, backgroundColor: "#4e40b8", justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ fontSize: 64, color: "#eeeeee" }}> AlarmSqlite </Text>
                        <Text style={{ fontSize: 24, color: "#eeeeee" }} > alarmy</Text>
                        <MyButton width="100%" text="Start" click={this.handleStart} />
                    </View>
                </React.Fragment>



        );
    }
}
export default Start;
