import React, { Component } from 'react';
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("matuszykrafal4ib2.db"); // proszę o taki schemat nazywania swojej bazy danych

class Database extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    static createTable() {
        db.transaction(tx => {
            tx.executeSql(
                "CREATE TABLE IF NOT EXISTS days2 (id integer primary key not null, data text);"
            );
        });
    }
    static getAll() {
        var query = "SELECT * FROM days2";
        return new Promise((resolve, reject) => db.transaction((tx) => {
            tx.executeSql(query, [], (tx, results) => {

                //console.log(JSON.stringify(results))

                resolve(JSON.stringify(results));

            }, function (tx, error) {

                reject(error);

            });
        }))
    }
    static add(h, m) {
        var tab = {
            hour: h,
            minute: m,
            switched: false,
            days: [{ day: "PN", clicked: false }, { day: "WT", clicked: false }, { day: "SR", clicked: false }, { day: "CZ", clicked: false }, { day: "PT", clicked: false }, { day: "SB", clicked: false }, { day: "ND", clicked: false }]
        };
        db.transaction(
            tx => {
                tx.executeSql("INSERT INTO days2 (data) values ('" + JSON.stringify(tab) + "')");
            },
        )
    }
    static remove(id) {

        db.transaction(tx => {
            tx.executeSql(
                "DELETE FROM days2 WHERE (id = " + id + ");"
            );
        });
    }

    static updateSwitch(data, id) {

        var tab = data;
        tab.switched = !tab.switched
        // console.log(tab)
        db.transaction(tx => {
            tx.executeSql(
                "UPDATE days2 SET data=('" + JSON.stringify(tab) + "') WHERE (id = " + id + ");"
            );
        });
    }
}

export default Database;
