import { createStackNavigator, createAppContainer } from "react-navigation";
import Start from "./components/Start";
import Main from "./components/Main";
import NewAlarm from "./components/NewAlarm";


const Root = createStackNavigator({
    start: { screen: Start },
    main: { screen: Main },
    newalarm: { screen: NewAlarm }
});

const App = createAppContainer(Root);

export default App;